package net.synergyserver.synergycore.utils;

import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.guis.Itemizable;
import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Utility that handles item-based GUIs.
 */
public class ItemGUIUtil {

    /**
     * Creates the item to display in an item-based GUI for the given <code>Itemizable</code> and parameter.
     *
     * @param itemizable The <code>Itemizable</code> of the item.
     * @param parameter The parameter to pass to the <code>Itemizable</code>.
     * @return The <code>ItemStack</code> to use in a GUI.
     */
    public static ItemStack itemize(Itemizable itemizable, Object parameter) {
        ItemStack item = itemizable.getItem(parameter);
        ItemMeta im = item.getItemMeta();

        // Hide all attributes
        im.addItemFlags(
                ItemFlag.HIDE_ENCHANTS,
                ItemFlag.HIDE_ATTRIBUTES,
                ItemFlag.HIDE_UNBREAKABLE,
                ItemFlag.HIDE_DESTROYS,
                ItemFlag.HIDE_PLACED_ON,
                ItemFlag.HIDE_POTION_EFFECTS
        );

        // Split the lore if it's too long
        List<String> newLore = new ArrayList<>();
        int maxLength = PluginConfig.getConfig(SynergyCore.getPlugin()).getInt("gui.max_length");
        for (String line : im.getLore()) {
            StringBuilder currentLine = new StringBuilder(line);

            while (currentLine.length() > maxLength) {

                // Figure out what colors to use for the next line
                String nextLineColors = ChatColor.getLastColors(currentLine.toString());

                // Find the end of where to split. If no word ends are found, split at the max length
                int splitEnd = maxLength - 1;
                if (currentLine.lastIndexOf(" ") != -1) {
                    splitEnd = currentLine.lastIndexOf(" ", splitEnd);
                }

                // Add the split line to the new lore and remove it from currentLine
                newLore.add(currentLine.substring(0, splitEnd + 1));
                currentLine = currentLine.delete(0, splitEnd + 1).insert(0, nextLineColors);
            }

            // Add the remainder of currentLine to the new lore
            newLore.add(currentLine.toString());
        }

        im.setLore(newLore);
        item.setItemMeta(im);
        return item;
    }

    /**
     * Determines where items should be placed in a GUI, based on the given parameters. There is two main patterns
     * that this algorithm tries to target: Lattice and full-fill. The lattice pattern is preferred, as it guarantees
     * white space in between items, which allows the cursor to reside without tooltips appearing. This algorithm also
     * favors the top, as it is simpler to implement, and is more visually pleasing. Also see the
     * <code>arrangeElements</code> method.
     *
     * @param maxWidth The maximum width that the grid can have.
     * @param maxHeight The maximum height that the grid can have.
     * @param numElements The number of elements that will eventually populate the grid.
     * @return A two-dimensional array of boolean values, where true indicates that an element should be placed there.
     */
    public static boolean[][] arrangeGrid(int maxWidth, int maxHeight, int numElements) {

        int rowOddEven = maxHeight % 2;
        int colOddEven = maxWidth % 2;

        // Determine whether to use the lattice pattern or the full-fill pattern.
        // Even width:
        // If the row count is even then the max lattice can hold is maxWidth * (1/2 * maxHeight)
        // If the row count is odd, then it is the same as above but then add an additional maxWidth * 1/2
        // Odd width:
        // Same as the above, but for odd row count then also add an additional +1
        int maxLattice = maxWidth * (maxHeight / 2);
        if (rowOddEven == 1) {
            maxLattice = (maxWidth / 2) + colOddEven;
        }

        List<Integer> elementDistribution = new ArrayList<>();

        if (numElements <= maxLattice) {
            // Lattice pattern
            int maxPerLargeRow = (maxWidth + 1) / 2;

            // Fill rows up to the max, but keep in mind that every other row can only hold 1 less than maxPerLargeRow
            int totalLeft = numElements;
            for (int i = 0; i < maxHeight && numElements > 0; i++) {
                int rowElements;
                if (i % 2 == 0) {
                    rowElements = totalLeft >= maxPerLargeRow ? maxPerLargeRow : totalLeft;
                } else {
                    rowElements = totalLeft >= maxPerLargeRow - 1 ? maxPerLargeRow - 1 : totalLeft;
                }
                if (rowElements != 0) {
                    elementDistribution.add(rowElements);
                }
                totalLeft -= rowElements;
            }
        } else {
            // Full-fill pattern
            int totalLeft = numElements;
            for (int i = 0; i < maxHeight && numElements > 0; i++) {
                int rowElements = totalLeft >= maxWidth ? maxWidth : totalLeft;
                if (rowElements != 0) {
                    elementDistribution.add(rowElements);
                }
                totalLeft -= rowElements;
            }
        }

        // Determine how to space the rows vertically
        boolean[] rowSpacing = arrangeElements(maxHeight, elementDistribution.size());

        // Arrange each row and store it in the 2d array to output
        boolean[][] grid = new boolean[maxHeight][maxWidth];
        int elementDistributionIndex = 0;
        for (int i = 0; i < maxHeight; i++) {
            if (!rowSpacing[i]) {
                Arrays.fill(grid[i], false);
                continue;
            }
            grid[i] = arrangeElements(maxWidth, elementDistribution.get(elementDistributionIndex));
            elementDistributionIndex++;
        }

        return grid;
    }

    /**
     * Determines where items should be placed in a row/collumn of a GUI, based on the given parameters. There are
     * three main patterns that this algorithm tries to target: Space-around, space-between, and clumping. Space-around
     * and space-between are preferred, as they guarantees white space in between items, which allows the cursor to
     * reside without tooltips appearing. This algorithm also favors the top and left, as it is simpler to implement,
     * and is more visually pleasing. In any algorithm that this method uses, it tries to center the elements as best
     * as it can.
     *
     * @param maxLength The maximum length.
     * @param numElements The number of elements that will eventually populate the array.
     * @return An array of boolean values, where true indicates that an element should be placed there.
     */
    public static boolean[] arrangeElements(int maxLength, int numElements) {
        boolean[] row = new boolean[maxLength];
        Arrays.fill(row, false);

        // Space-around
        int saSpaceSpots = numElements + 1;
        if (saSpaceSpots + numElements <= maxLength) {
            int spaces = maxLength - numElements;
            int spacer = spaces / saSpaceSpots;

            // If the spacer doesn't divide evenly then set it to 1
            if (spaces % spacer != 0) {
                spacer = 1;
            }

            int leftExtraSpaces = (maxLength - (saSpaceSpots * spacer + numElements)) / 2;

            // Add half of the remaining spaces on the left end, then place an item marker at the end of each spacer
            for (int i = leftExtraSpaces + spacer; i < leftExtraSpaces + (spacer + 1) * numElements; i += (spacer + 1)) {
                row[i] = true;
            }
            return row;
        }

        // Space-between instead
        int sbSpaceSpots = numElements - 1;
        if (numElements + sbSpaceSpots == maxLength) {
            // The spacer should be 1, so place an item in every other spot
            for (int i = 0; i < maxLength; i += 2) {
                row[i] = true;
            }
            return row;
        }

        // Else, clump at the center
        return centerClumpElements(maxLength, numElements);
    }

    /**
     * Clumps elements at the center of an array.
     *
     * @param maxLength The maximum length.
     * @param numElements The number of elements that will eventually populate the array.
     * @return An array of boolean values, where true indicates that an element should be placed there.
     */
    public static boolean[] centerClumpElements(int maxLength, int numElements) {
        boolean[] row = new boolean[maxLength];
        Arrays.fill(row, false);

        int center = maxLength / 2;
        int onLeft = numElements / 2;
        int onRight = (numElements - 1) / 2;
        // Make it favor left for odd #of elements with even width
        if (maxLength % 2 == 0 && numElements % 2 == 1) {
            center--;
        }
        for (int i = center - onLeft; i <= center + onRight; i++) {
            row[i] = true;
        }
        return row;
    }

    /**
     * Converts indexes of a two dimensional array to the corresponding index
     * as if it were treated as a one dimensional array with wrapping.
     *
     * @param row The index of the row.
     * @param column The index of the column.
     * @param width The width of the 2d array.
     * @return The corresponding index in a 1d array with wrapping.
     */
    public static int toOneDimensionalIndex(int row, int column, int width) {
        return width * row + column;
    }

    /**
     * Converts the index of a one dimensional array to the Y index of a two dimensional
     * array with the given width, if the one dimensional array had wrapping.
     *
     * @param index The index of the 1d array.
     * @param width The width of the 2d array.
     * @return The Y index of the 2d array.
     */
    public static int toTwoDimensionalIndexY(int index, int width) {
        return index / width;
    }

    /**
     * Converts the index of a one dimensional array to the X index of a two dimensional
     * array with the given width, if the one dimensional array had wrapping.
     *
     * @param index The index of the 1d array.
     * @param width The width of the 2d array.
     * @return The X index of the 2d array.
     */
    public static int toTwoDimensionalIndexX(int index, int width) {
        return index % width;
    }
}
