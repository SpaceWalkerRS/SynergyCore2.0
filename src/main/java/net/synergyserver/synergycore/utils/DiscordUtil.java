package net.synergyserver.synergycore.utils;

import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.exceptions.ServiceOfflineException;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.StatusType;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MessageBuilder;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

/**
 * Utility that handles Discord related things.
 */
public class DiscordUtil {

    /**
     * Checks if the user is staff in the specified guild.
     *
     * @param user The user to be checked.
     * @param guild The guild to check against.
     * @return True if the user is staff.
     */
    public static boolean isStaff(IUser user, IGuild guild) {
        return user.getRolesForGuild(guild).contains(guild.getRolesByName("Admin").get(0)) || user.getRolesForGuild(guild).contains(guild.getRolesByName("Helper").get(0));
    }

    /**
     * Sends a given message in a specified channel.
     *
     * @param client The <code>IDiscordClient</code> the bot is running off of.
     * @param channel The <code>IChannel</code> to send the message in.
     * @param message The <code>String</code> sent to the channel.
     *
     * @return The <code>IMessage</code> to send.
     */
    public static IMessage sendMessage(IDiscordClient client, IChannel channel, String message) throws RateLimitException, DiscordException, MissingPermissionsException {
        return new MessageBuilder(client).withChannel(channel).withContent(message).build();
    }

    /**
     * Checks if the given user is online.
     *
     * @param discordID The id of the user to check.
     * @return True if the user is online.
     * @throws ServiceOfflineException if the integration is offline.
     */
    public static boolean isOnline(String discordID) throws ServiceOfflineException {
        IDiscordClient discordClient = SynergyCore.getDiscordClient();
        return discordClient.getUserByID(Long.parseLong(discordID)).getPresence().getStatus().equals(StatusType.ONLINE);
    }
}
