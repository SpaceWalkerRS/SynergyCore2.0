package net.synergyserver.synergycore.events;

import net.synergyserver.synergycore.MemberApplication;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * This event is called whenever a <code>MemberApplication</code> is submitted.
 */
public class MemberApplicationSubmitEvent extends Event {

    private MemberApplication memberApplication;
    private static final HandlerList handlers = new HandlerList();

    /**
     * Creates a new <code>MemberApplicationSubmitEvent</code> with the given <code>MemberApplication</code>.
     *
     * @param memberApplication The application of this event.
     */
    public MemberApplicationSubmitEvent(MemberApplication memberApplication) {
        this.memberApplication = memberApplication;
    }

    /**
     * Gets the <code>MemberApplication</code> of this event.
     *
     * @return The application of this event.
     */
    public MemberApplication getMemberApplication() {
        return memberApplication;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
