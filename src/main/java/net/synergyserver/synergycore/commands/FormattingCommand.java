package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.configs.TextConfig;
import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "formatting",
        aliases = {"colors", "colours", "codes"},
        permission = "syn.colors",
        usage = "/colors",
        description = "Displays the formatting codes for formatting text.",
        maxArgs = 0
)
public class FormattingCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        for (String line : TextConfig.getText("formatting.txt")) {
            sender.sendMessage(line);
        }
        return true;
    }
}
