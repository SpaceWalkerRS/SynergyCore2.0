package net.synergyserver.synergycore.commands;

import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "donator",
        aliases = {"managedonators", "md"},
        permission = "syn.donator",
        usage = "/donator <amount|updatestatus|subscriptionstatus>",
        description = "Main command for managing donators."
)
public class DonatorCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }
}
