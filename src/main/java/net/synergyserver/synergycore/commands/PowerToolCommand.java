package net.synergyserver.synergycore.commands;

import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "powertool",
        aliases = {"pow", "tool"},
        permission = "syn.powertool",
        usage = "/powertool <add|remove|list|clear>",
        description = "Main command for managing PowerTools.",
        validSenders = SenderType.PLAYER
)
public class PowerToolCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }
}
