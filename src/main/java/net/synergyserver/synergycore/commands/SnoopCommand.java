package net.synergyserver.synergycore.commands;

import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "snoop",
        aliases = "sn",
        permission = "syn.snoop",
        usage = "/snoop <add|remove|list>",
        description = "Main command for managing snooped players.",
        validSenders = SenderType.PLAYER
)
public class SnoopCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }
}
