package net.synergyserver.synergycore.commands;

import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "preset",
        aliases = "presets",
        permission = "syn.settings.preset",
        usage = "/settings preset <load|list|create|delete>",
        description = "Main command for managing your setting presets.",
        validSenders = SenderType.PLAYER,
        parentCommandName = "settings"
)
public class SettingsPresetCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }
}
