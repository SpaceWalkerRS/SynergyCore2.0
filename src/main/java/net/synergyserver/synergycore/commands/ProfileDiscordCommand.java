package net.synergyserver.synergycore.commands;

import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.exceptions.ServiceOfflineException;
import net.synergyserver.synergycore.profiles.DiscordProfile;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.SynUser;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.IVoiceChannel;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@CommandDeclaration(
        commandName = "discord",
        aliases = "dis",
        permission = "syn.profile.discord",
        usage = "/profile discord [player]",
        description = "Gets the Discord profile for a player.",
        maxArgs = 1,
        parentCommandName = "profile"
)
public class ProfileDiscordCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        SynUser synUser;
        MinecraftProfile mcp;

        if (args.length == 0) {
            // If the sender isn't a player, then an argument is required
            if (!SenderType.getSenderType(sender).equals(SenderType.PLAYER)) {
                sender.sendMessage(Message.get("commands.error.wrong_sender_type"));
                return false;
            }

            mcp = PlayerUtil.getProfile(((Player) sender));
            synUser = DataManager.getInstance().getDataEntity(SynUser.class, mcp.getSynID());
        } else {
            UUID pID = PlayerUtil.getUUID(args[0], true, sender.hasPermission("vanish.see"));

            if (pID == null) {
                sender.sendMessage(Message.format("commands.error.player_not_found", args[0]));
                return false;
            }

            // Get the most current profile
            if (PlayerUtil.isOnline(pID)) {
                mcp = PlayerUtil.getProfile(pID);
            } else {
                mcp = DataManager.getInstance().getPartialDataEntity(MinecraftProfile.class, pID, "sid", "n");
            }

            synUser = DataManager.getInstance().getPartialDataEntity(SynUser.class, mcp.getSynID(), "d");
        }

        String color1 = Message.get("info_colored_lists.item_color_1");
        String grammarColor = Message.get("info_colored_lists.grammar_color");

        // Send the header
        sender.sendMessage(Message.format("commands.profile.info.header", mcp.getCurrentName(), "Discord"));

        DiscordProfile discordProfile = synUser.getDiscordProfile();
        if (discordProfile != null) {
            sender.sendMessage(Message.format("commands.profile.discord.detail.display_name", discordProfile.getCurrentName()));
            sender.sendMessage(Message.format("commands.profile.discord.detail.id", discordProfile.getID()));

            try {
                IUser user = discordProfile.getUser();
                IGuild guild = SynergyCore.getDiscordServer();

                String onlineStatus = discordProfile.isOnline() ? ChatColor.GREEN + "Online" : ChatColor.DARK_RED + "Offline";
                long time = discordProfile.isOnline() ? discordProfile.getLastLogIn() : discordProfile.getLastLogOut();
                String onlineTimeMessage = Message.getTimeMessage(System.currentTimeMillis() - time, 3, 1, color1, grammarColor);
                sender.sendMessage(Message.format("commands.profile.discord.detail.online_status", onlineStatus, onlineTimeMessage));

                sender.sendMessage(Message.format("commands.profile.discord.detail.ban_status", discordProfile.isBanned() ? "Banned" : "Not banned"));

                List<String> roles = guild.getRolesForUser(user).stream().map(IRole::getName).collect(Collectors.toList());
                String rolesMessage = Message.createFormattedList(roles, color1, grammarColor);
                sender.sendMessage(Message.format("commands.profile.discord.detail.roles", rolesMessage));

                IVoiceChannel voiceChannel = user.getVoiceStateForGuild(guild).getChannel();
                if (voiceChannel != null) {
                    sender.sendMessage(Message.format("commands.profile.discord.detail.voice_channel", voiceChannel.getName()));
                }
            } catch (ServiceOfflineException e) {
                sender.sendMessage(Message.format("commands.error.service_offline", "Discord"));
            }
        } else {
            sender.sendMessage(Message.format("commands.profile.error.service_not_connected", mcp.getCurrentName(), "Discord"));
            return false;
        }

        return true;
    }
}
