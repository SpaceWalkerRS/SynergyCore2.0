package net.synergyserver.synergycore;

/**
 * Represents a type of weather.
 */
public enum WeatherType {
    CLEAR("clear", false, false, false),
    RAIN("rain", true, false, false),
    RAIN_STORM("rainstorm", true, true, false),
    SNOW("snow", true, false, true),
    SNOW_STORM("snowstorm", true, true, true);

    private String displayText;
    private boolean isStorm;
    private boolean isThundering;
    private boolean isSnowy;

    WeatherType(String displayText, boolean isStorm, boolean isThundering, boolean isSnowy) {
        this.displayText = displayText;
        this.isStorm = isStorm;
        this.isThundering = isThundering;
        this.isSnowy = isSnowy;
    }

    /**
     * Gets the text that represents this <code>WeatherType</code> in chat.
     *
     * @return The display text.
     */
    public String getDisplayText() {
        return displayText;
    }

    /**
     * Checks whether this <code>WeatherType</code> represents a storm.
     *
     * @return True if this is a storm.
     */
    public boolean isStorm() {
        return isStorm;
    }

    /**
     * Checks whether this <code>WeatherType</code> represents a thunderstorm.
     *
     * @return True if this is a thunderstorm.
     */
    public boolean isThundering() {
        return isThundering;
    }

    /**
     * Checks whether this <code>WeatherType</code> represents a snowstorm.
     *
     * @return True if this is a snowstorm.
     */
    public boolean isSnowy() {
        return isSnowy;
    }

    /**
     * Gets the Bukkit <code>WeatherType</code> of this <code>WeatherType</code>.
     *
     * @return The Bukkit <code>WeatherType</code>.
     */
    public org.bukkit.WeatherType toBukkitWeatherType() {
        return this.isStorm ? org.bukkit.WeatherType.DOWNFALL : org.bukkit.WeatherType.CLEAR;
    }

    public static WeatherType getWeatherType(boolean isStorm, boolean isThundering, boolean isSnowy) {
        for (WeatherType weatherType : WeatherType.values()) {
            if (weatherType.isStorm() == isStorm && weatherType.isThundering() == isThundering && weatherType.isSnowy() == isSnowy) {
                return weatherType;
            }
        }
        return null;
    }

    /**
     * Gets the <code>WeatherType</code> of a Bukkit <code>WeatherType</code>.
     *
     * @param weatherType The Bukkit <code>WeatherType</code>.
     * @param wouldSnow True it would be snowing if there were rain.
     * @return The <code>WeatherType</code>.
     */
    public static WeatherType fromBukkitWeatherType(org.bukkit.WeatherType weatherType, boolean wouldSnow) {
        switch (weatherType) {
            case CLEAR: return CLEAR;
            case DOWNFALL:
                if (wouldSnow) {
                    return SNOW;
                } else {
                    return RAIN;
                }
            default: return null;
        }
    }

    /**
     * Parses a <code>WeatherType</code> from the given string.
     *
     * @param toParse The string to parse a <code>WeatherType</code> from.
     * @return The <code>WeatherType</code>, or null if no weather was able to be parsed.
     */
    public static WeatherType parseWeatherConstant(String toParse) {
        switch (toParse.toLowerCase()) {
            case "sun": case "clear":
                return CLEAR;
            case "rain": case "downfall":
                return RAIN;
            case "storm": case "thunderstorm": case "thor":
                return RAIN_STORM;
            case "snow":
                return SNOW;
            case "snowstorm": case "elsa":
                return SNOW_STORM;
            default: return null;
        }
    }
}
