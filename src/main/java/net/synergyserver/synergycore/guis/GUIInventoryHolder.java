package net.synergyserver.synergycore.guis;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

/**
 * Used to determine if an inventory belongs to a <code>GUI</code>.
 */
public class GUIInventoryHolder implements InventoryHolder {

    private GUI gui;
    private GUIView view;

    /**
     * Creates a new <code>GUIInventoryHolder</code>.
     */
    public GUIInventoryHolder() {}

    /**
     * Creates a new <code>GUIInventoryHolder</code> with the given parameters.
     *
     * @param gui The <code>GUI</code> of this <code>GUIInventoryHolder</code>.
     * @param view The <code>GUIView</code> of the inventory of this <code>GUIInventoryHolder</code>
     */
    public GUIInventoryHolder(GUI gui, GUIView view) {
        this.gui = gui;
        this.view = view;
    }

    /**
     * Gets the <code>GUI</code> of this <code>GUIInventoryHolder</code>.
     *
     * @return The <code>GUI</code> of this <code>GUIInventoryHolder</code>.
     */
    public GUI getGui() {
        return gui;
    }

    /**
     * Sets the <code>GUI</code> of this <code>GUIInventoryHolder</code>.
     *
     * @param gui The new <code>GUI</code> of this <code>GUIInventoryHolder</code>.
     */
    public void setGui(GUI gui) {
        this.gui = gui;
    }

    /**
     * Gets the <code>GUIView</code> of this <code>GUIInventoryHolder</code>.
     *
     * @return The <code>GUIView</code> of this <code>GUIInventoryHolder</code>.
     */
    public GUIView getView() {
        return view;
    }

    /**
     * Sets the <code>GUIView</code> of this <code>GUIInventoryHolder</code>.
     *
     * @param view The new <code>GUIView</code> of this <code>GUIInventoryHolder</code>.
     */
    public void setView(GUIView view) {
        this.view = view;
    }

    /**
     * This shouldn't ever be called, but just in case return the view's inventory to avoid an error.
     *
     * @return The view's inventory.
     */
    public Inventory getInventory() {
        return view.getInventory();
    }
}
