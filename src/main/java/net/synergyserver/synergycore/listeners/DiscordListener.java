package net.synergyserver.synergycore.listeners;

import net.synergyserver.synergycore.ServiceToken;
import net.synergyserver.synergycore.ServiceType;
import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.events.ServiceConnectEvent;
import net.synergyserver.synergycore.profiles.DiscordProfile;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.profiles.SynUser;
import net.synergyserver.synergycore.utils.DiscordUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.impl.events.guild.member.UserJoinEvent;
import sx.blah.discord.handle.impl.events.guild.member.UserLeaveEvent;
import sx.blah.discord.handle.impl.events.user.PresenceUpdateEvent;
import sx.blah.discord.handle.impl.events.user.UserUpdateEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RequestBuffer;

import java.util.UUID;

/**
 * Listens to events that occur on the Discord server.
 */
public class DiscordListener {

    private static DiscordListener instance = null;

    /**
     * Creates a new <code>DiscordListener</code> object.
     */
    private DiscordListener() {}

    /**
     * Returns the object representing this <code>DiscordListener</code>.
     *
     * @return The object of this class.
     */
    public static DiscordListener getInstance() {
        if (instance == null) {
            instance = new DiscordListener();
        }
        return instance;
    }

    @EventSubscriber
    public void onMessageReceive(MessageReceivedEvent event) {
        IMessage message = event.getMessage();
        IChannel channel = message.getChannel();
        IGuild guild = message.getGuild();
        IDiscordClient client = message.getClient();
        IUser user = message.getAuthor();
        DataManager dm = DataManager.getInstance();

        // Ignore the event if it occurred in the wrong channel
        if (channel.getLongID() != SynergyCore.getPluginConfig().getLong("discord.gatechannel")) {
            return;
        }

        // Ignore this event if the Discord account is already connected to a user
        if (dm.isDataEntityInDatabase(DiscordProfile.class, user.getStringID())) {
            return;
        }

        for (Player player : Bukkit.getOnlinePlayers()) {
            MinecraftProfile mcp = PlayerUtil.getProfile(player);
            SynUser synUser = dm.getDataEntity(SynUser.class, mcp.getSynID());
            ServiceToken token = synUser.getDiscordToken();

            if (token != null && token.getToken().equals(message.getContent())) {
                // If the token is expired then remove the token from the database
                if (token.isExpired()) {
                    synUser.setDiscordToken(null);
                    break;
                }

                // Send a success message
                RequestBuffer.request(() -> {
                    try {

                        // Create their DiscordProfile and connect it
                        String newNickname = mcp.getCurrentName();

                        if (!DiscordUtil.isStaff(user, guild)) {
                            user.addRole(guild.getRolesByName("Player").get(0));
                            guild.setUserNickname(user, newNickname);
                        }

                        DiscordUtil.sendMessage(client, guild.getChannelByID(SynergyCore.getPluginConfig().getLong("discord.mainchannel")),
                                Message.format("service_connection.discord.join", newNickname));
                    } catch (DiscordException|MissingPermissionsException e) {
                        e.printStackTrace();
                    }
                });

                synUser.setDiscordID(user.getStringID());

                DiscordProfile discordProfile = new DiscordProfile(
                        synUser.getID(),
                        user.getStringID(),
                        user.getName()
                );

                dm.saveDataEntity(discordProfile);

                // Emit an event
                ServiceConnectEvent serviceConnectEvent = new ServiceConnectEvent(synUser, ServiceType.DISCORD, discordProfile);
                Bukkit.getPluginManager().callEvent(serviceConnectEvent);
                break;
            }
        }

        // Delete the message after everything is done
        message.delete();
    }

    @EventSubscriber
    public void onUserJoin(UserJoinEvent event) {
        DataManager dm = DataManager.getInstance();
        IUser user = event.getUser();
        IGuild server = event.getGuild();
        DiscordProfile discordProfile = dm.getDataEntity(DiscordProfile.class, user.getStringID());

        if (discordProfile == null) {
            return;
        }

        // Update the DiscordProfile's known names if they're using a new name
        if (!discordProfile.getCurrentName().equals(user.getName())) {
            discordProfile.setCurrentName(user.getName());
            discordProfile.addKnownName(user.getName());
        }

        // Update the player's nickname based on their Minecraft username
        UUID pID = dm.getDataEntity(SynUser.class, discordProfile.getSynID()).getMinecraftID();
        MinecraftProfile mcp = dm.getPartialDataEntity(MinecraftProfile.class, pID, "n");

        if (mcp == null) {
            return;
        }

        if (!user.getNicknameForGuild(server).equals(mcp.getCurrentName())) {
            server.setUserNickname(user, mcp.getCurrentName());
        }

        // Update the login time
        discordProfile.setLastLogIn(System.currentTimeMillis());
    }

    @EventSubscriber
    public void onUserLeave(UserLeaveEvent event) {
        DataManager dm = DataManager.getInstance();
        IUser user = event.getUser();
        DiscordProfile discordProfile = dm.getDataEntity(DiscordProfile.class, user.getStringID());

        if (discordProfile == null) {
            return;
        }

        discordProfile.setLastLogOut(System.currentTimeMillis());
    }

    @EventSubscriber
    public void onPresenceUpdate(PresenceUpdateEvent event) {
        DataManager dm = DataManager.getInstance();
        IUser user = event.getUser();
        DiscordProfile discordProfile = dm.getDataEntity(DiscordProfile.class, user.getStringID());

        if (discordProfile == null) {
            return;
        }

        switch (event.getNewPresence().getStatus()) {
            case ONLINE:
                // Update the login time
                discordProfile.setLastLogIn(System.currentTimeMillis());
                break;
            case OFFLINE: case INVISIBLE:
                // Update the log out time
                discordProfile.setLastLogOut(System.currentTimeMillis());
                break;
            default: break;
        }
    }

    @EventSubscriber
    public void onUserUpdate(UserUpdateEvent event) {
        DataManager dm = DataManager.getInstance();
        IUser user = event.getUser();
        IGuild server = SynergyCore.getDiscordServer();
        DiscordProfile discordProfile = dm.getDataEntity(DiscordProfile.class, user.getStringID());

        if (discordProfile == null) {
            return;
        }

        // Update the DiscordProfile's known names if they're using a new name
        if (!discordProfile.getCurrentName().equals(user.getName())) {
            discordProfile.setCurrentName(user.getName());
            discordProfile.addKnownName(user.getName());
        }

        // Update the player's nickname based on their Minecraft username
        UUID pID = dm.getDataEntity(SynUser.class, discordProfile.getSynID()).getMinecraftID();
        MinecraftProfile mcp = dm.getPartialDataEntity(MinecraftProfile.class, pID, "n");

        if (mcp == null) {
            return;
        }

        if (!mcp.getCurrentName().equals(user.getNicknameForGuild(server))) {
            server.setUserNickname(user, mcp.getCurrentName());
        }
    }
}
