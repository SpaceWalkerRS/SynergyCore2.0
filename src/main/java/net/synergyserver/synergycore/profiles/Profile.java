package net.synergyserver.synergycore.profiles;

import net.synergyserver.synergycore.database.DataEntity;

/**
 * Represents a player's data for some service.
 */
public interface Profile extends DataEntity {

    /**
     * Gets the last time the player logged onto the corresponding service of this profile.
     *
     * @return A long representing the time the player logged in, in milliseconds since midnight, January 1, 1970 UTC.
     */
    long getLastLogIn();

    /**
     * Sets the time the played logged onto the corresponding service of this profile.
     *
     * @param logInTime A long representing the time the player logged in,
     *                  in milliseconds since midnight, January 1, 1970 UTC.
     */
    void setLastLogIn(long logInTime);

    /**
     * Gets the time the played logged out of the corresponding service of this profile.
     *
     * @return A long representing the time the player logged out, in milliseconds since midnight, January 1, 1970 UTC.
     */
    long getLastLogOut();

    /**
     * Sets the time the played logged out of the corresponding service of this profile.
     *
     * @param logOutTime A long representing the time the player logged out,
     *                  in milliseconds since midnight, January 1, 1970 UTC.
     */
    void setLastLogOut(long logOutTime);

    /**
     * Checks if the player is banned from using the corresponding service of this profile.
     *
     * @return True if the player is banned.
     */
    boolean isBanned();

    /**
     * Sets the ban status of the player for the corresponding service of this profile.
     *
     * @param banStatus True if the player should be banned.
     */
    void setIsBanned(boolean banStatus);

}
