package net.synergyserver.synergycore;

import net.synergyserver.synergycore.database.DataEntity;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;

import java.util.UUID;

/**
 * Represents a player's application for Member.
 */
@Entity(value = "memberapplications")
public class MemberApplication implements DataEntity {

    @Id
    private ObjectId appID;
    @Property("pid")
    private UUID pID;
    @Property("t")
    private long timeSent;

    /**
     * Required constructor for Morphia to work.
     */
    public MemberApplication() {}

    /**
     * Creates a new <code>MemberApplication</code>.
     *
     * @param pID The ID of the player who submitted this application.
     * @param timeSent The time that this application was submitted, as milliseconds since the epoch.
     */
    public MemberApplication(UUID pID, long timeSent) {
        this.appID = new ObjectId();
        this.pID = pID;
        this.timeSent = timeSent;
    }

    @Override
    public ObjectId getID() {
        return appID;
    }

    /**
     * Gets the Minecraft player ID of the player who submitted this application.
     *
     * @return The ID of the player of this application.
     */
    public UUID getPlayerID() {
        return pID;
    }

    /**
     * Gets the time that this application was submitted, as milliseconds since the unix epoch.
     *
     * @return The time that this application was submitted.
     */
    public long getTimeSent() {
        return timeSent;
    }
}
